// NUMBER 2 - COUNT OPERATOR
db.fruits.aggregate( [
	{ $match: {"onSale": true } },
	{ $count: "fruitsOnSale"}
]);

// NUMBER 3 - COUNT THE TOTAL NUMBER  OF FRUITS WITH STOCK MORE THAN 20
db.fruits.aggregate( [
	{ $match: {"stock": { $gt: 20 } } },
	{ $count: "enoughStock"}
]);

// NUMBER 4 - Average operator
db.fruits.aggregate([
	{ $match: {"onSale": true } },
	{ $group: { "_id": "$supplier_id", "avg_price": {$avg: "$price" } } }
]);

// NUMBER 5 - MAX OPERATOR
db.fruits.aggregate([
	{ $match: {"onSale": true } },
	{ $group: { "_id": "$supplier_id", "max_price": {$max: "$price" } } }
]);

// NUMBER 6 - MIN OPERATOR 
db.fruits.aggregate([
	{ $match: {"onSale": true } },
	{ $group: { "_id": "$supplier_id", "min_price": {$min: "$price" } } }
]);